FROM golang:alpine as builder

# install progs
RUN apk --no-cache add git ca-certificates curl
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

#assert vals
ENV GOPATH /go
ENV PATH $GOPATH/bin:$PATH

# building
WORKDIR /go/src/app
ADD app /go/src/app
RUN dep ensure --vendor-only
RUN go get -d -v ./...
RUN go install -v ./... 

#new container
FROM justcontainers/base-alpine
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/bin/app /bin/

# We aren't cavemen
ENV S6_KEEP_ENV=1
COPY etc/services.d/app /etc/services.d/app/
RUN mkdir -p /var/lib/app/{data,logs}
VOLUME /var/lib/app

#connections
EXPOSE 8080

#run
ENTRYPOINT ["/init"]
CMD []

