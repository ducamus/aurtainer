package main

import (
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	log.Println("starting up")
	r := mux.NewRouter()
	r.HandleFunc("/", HandleIndex)
	port := ":8080"
	server := &http.Server{
		Handler:      r,
		Addr:         port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(server.ListenAndServe())
}
func HandleIndex(w http.ResponseWriter, r *http.Request) {
	log.Println("Handling request")

	io.WriteString(w, "hello")
}
